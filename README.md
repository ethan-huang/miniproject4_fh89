## Mini-Project-4

Welcome to the Random Number Generator Web Application! This project demonstrates a simple web service built with Actix Web and Rust to generate random numbers ranging from 1 to 100.

## Steps

- `cargo new ids721hw4` to create a folder named ids721hw4
- Go to `src/main.rs` to implement a function called generate_numbers ranging from 1 to 100
- include `use actix_web::{web, App, HttpResponse, HttpServer, Responder}` and use `actix_files::Files` on top of the file.
- in cargo.toml to include relevant dependencies `actix-web`: web framework for Rust

- include `actix-files`: handle serving static files

- include `rand`: generating random numbers in Rust

- create a docker file to have `FROM, WORKDIR, USER, COPY, RUN, EXPOSE, and CMD`

- `cargo build` and `cargo run` to test the main functions

- Run the docker container by running `docker run -p 8080:8080 <project_name>`

## Results

# Screenshots of cargo running

![Screenshot](image/cargorun.png)

# Screenshots of docker build

---

![Screenshot](image/dockerbuild.png)

# Screenshots of docker container

---

![Screenshot](image/week4.png)

# Screenshots of webpage on localhost

---

![Screenshot](image/result.png)

# Screenshots of running container

---

![Screenshot](image/dc.png)
