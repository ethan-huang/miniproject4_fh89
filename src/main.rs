use rand::Rng;
use actix_files::Files;
use actix_web::{web,App,HttpResponse, HttpServer, Responder};

// Dice rolling function using random number generator
async fn generate_numbers() -> impl Responder {
    let result = rand::thread_rng().gen_range(1..=100).to_string();
    HttpResponse::Ok().body(format!("is : {}", result))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/generate", web::get().to(generate_numbers))  // Add route for dice rolling
            .service(Files::new("/", "./static").index_file("index.html")) // Serve static files
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}

